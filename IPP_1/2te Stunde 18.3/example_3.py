def fak(n):
  # Startwert der Berechnung
  # Wir berechnen mindestens immer 0!
  i_n = 0   # i_n = Iterator i von 0 bis n
  # Hiermit merken wir uns jeweils den zuletzt berechneten
  # Fakultätswert
  n_fak = 1 # 0! = 1

  # wir überprüfen, ob wir n noch nicht erreicht haben ...
  while  i_n < n:
    # ... und solange gehen wir eine "Zeile weiter" => n! = (n-1)! n
    i_n = i_n + 1
    n_fak = n_fak * i_n

  return n_fak
# Kleinen Text ausgeben, damit der Nutzer weiß, was hier passiert

import math

datei_name = input("Gib mir einen Datein namen: ")
anzahl_s = int(input("Wie viele Stützstellen: "))
list_x2 = []
list_sin = []
list_x = range(0,anzahl_s)
fd = open(datei_name, 'w')
for i in list_x:
    list_x2.append(i*2*math.pi*(1/anzahl_s))
    list_sin.append(math.sin(list_x2[i]))
    fd.write('sin(' + str(list_x2[i]) + ") =" + str(list_sin[i]) + "\n")

fd.write(str(anzahl_s) + ' Datensätze in der Datei ' + datei_name +' gespeichert ')
fd.close