"""
Einfacher UDP-Server, der auf eine Nachricht wartet, eine Antwort sendet und sich dann beendet.
"""

# Für die Sockets gibt es natürlich ein eigenes Modul
import socket
import numpy as np
import matplotlib.pyplot as plt


class SinPlot():
  # Konstruktor
  def __init__(self):
    # Plot anlegen ...
    self.fig, self.ax = plt.subplots()

  # Aufruf für die erste Plot-Ausgabe
  def init_plot(self, t_vec, sin_vec):
    # Ersten Stützpunkt "normal" plotten ...
    self.sin_plot, = self.ax.plot(t_vec, sin_vec)
    # ... und das Diagramm groß genug machen für die weiteren Punkte
    self.ax.set_xlim(-1,2.0*np.pi)
    self.ax.set_ylim(-1.5,+1.5)

  # Aufruf für die weiteren Plot-Ausgaben
  def update_plot(self, t_vec, sin_vec):
    self.sin_plot.set_data(t_vec,sin_vec)

if __name__ == '__main__':
  n_gesamt = 1000
  t_vec = np.zeros(n_gesamt)
  # Vektor für Ergebniswerte
  sin_vec = np.zeros_like(t_vec)
  sinus_dtype = np.dtype([('id',np.int32),
                           ('x', np.float32),
                           ('y', np.float32)])

  # Wir nutzen das Loopback-Device zur rechnerinternen Kommunikation ...
  localIP     = "127.0.0.1"
  # ... und einen nicht belegten Port
  localPort   = 1234
  # Das ist die maximale Paketlänge für den Empfang ...
  bufferSize  = 1024
  # Jetzt legen wir einen Socket für die UDP an ...
  UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
  # ... und binden ihn an unseren Port ...
  UDPServerSocket.bind((localIP, localPort))

  # ... und schon können wir auf Empfang gehen
  # Die Funktion recvfrom() kehrt nur zurück, wenn eine Nachricht empfangen wurde
  # oder ein Fehler aufgetreten ist ...
  bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
  received_struct = np.frombuffer(bytesAddressPair, dtype=sinus_dtype)
  i=0     #1 Datensatz für die Initalisierung
  sin_plot = SinPlot()

  if(received_struct['id'] == 1):
    t_vec[i] = received_struct['x']
    sin_vec[i] = received_struct['y']
    sin_plot.init_plot(t_vec[:i], sin_vec[:i])
    plt.pause(0.1)

  bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)     #vorher einen Datensatz holen sodass die id=2 ist
  received_struct = np.frombuffer(bytesAddressPair, dtype=sinus_dtype)
  while(received_struct['id'] == 2):
    t_vec[i] = received_struct['x']
    sin_vec[i] = received_struct['y']
    if(i%10 == 0):
      sin_plot.update_plot(t_vec[:i], sin_vec[:i])
      plt.pause(0.1)
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    received_struct = np.frombuffer(bytesAddressPair, dtype=sinus_dtype)      #am Ende einen neuen holen, falls die while schleife durch sein soll
  plt.show()

  #if(received_struct['id'] == 3):
