# Aufgabe 2.1

# Kleinen Text ausgeben, damit der Nutzer weiß, was hier passiert
print('Berechnung von sin(x) mittels endlicher Reihe')
pi = float(3.1415936)
# Wir lesen x von der Konsole ein, dabei können wir 'input()' einen
# String übergeben, mit dem wir dem Nutzer sagen, was wir von ihm wollen
x = float(input('Geben Sie x in Grad ein! '))
x *= pi/180
# und dann die sin-Näherung mit den ersten 3 Reihengliedern berechnen
sin_x = x/1 - (x*x*x)/(3*2*1) + (x*x*x*x*x)/(5*4*3*2*1)

# augeben sollten wir das Ergebnis natürlich auch noch
print('sin(',x,') = ', sin_x)
