"""
Iterative Berechnung der Sinusfunktion mit schritthaltender grafischer Ausgabe
"""
# Importieren der notwendigen Module
import numpy as np
import matplotlib.pyplot as plt
# Unsere Klasse für die Plotausgabe
# Unsere Klasse für die Sinus-Berechnung
class Sinus():
  def __init__(self, A=1.0, omega=1.0):
    # Amplitude
    self.A = A
    # Kreisfrequenz
    self.omega = omega

  # "Setter"-Funktion für die Amplitude
  def set_A(self, A):
    self.A = A

  # "Setter"-Funktion für die Kreisfrequenz
  def set_omega(self, omega):
    self.omega = omega

  # Berechnung des Sinus
  def calc(self,t):
    return self.A * np.sin(self.omega*t)

# Unsere Klasse für die Plotausgabe
class SinPlot():
  # Konstruktor
  def __init__(self):
    # Plot anlegen ...
    self.fig, self.ax = plt.subplots()

  # Aufruf für die erste Plot-Ausgabe
  def init_plot(self, t_vec, sin_vec):
    # Ersten Stützpunkt "normal" plotten ...
    self.sin_plot, = self.ax.plot(t_vec, sin_vec)
    # ... und das Diagramm groß genug machen für die weiteren Punkte
    self.ax.set_xlim(-1,2.0*np.pi)
    self.ax.set_ylim(-1.5,+1.5)

  # Aufruf für die weiteren Plot-Ausgaben
  def update_plot(self, t_vec, sin_vec):
    self.sin_plot.set_data(t_vec,sin_vec)

#---------------------------------------------
# Hauptprogramm: Einstiegspunkt
if __name__ == '__main__':
  # Anzahl der Stützpunkte
  n_gesamt = 1000
  # Darstellungsintervall
  t_gesamt = 2.0*np.pi
  # Inkrement zwischen 2 Stützpunkten
  dt = t_gesamt / n_gesamt

  # Vektor für Zeitwerte
  t_vec = np.zeros(n_gesamt)
  # Vektor für Ergebniswerte
  sin_vec = np.zeros_like(t_vec)

  # Sinus-Objekt anlegen ...
  sinus = Sinus()
  # Plot-Objekt anlegen ...
  sin_plot = SinPlot()

  # Wir ergänzen ein paar Parameter für die Sinus-Funktion
  omega = 0.0
  domega = 0.025
  A = 1.0
  dA = -0.001
  # ... und den ersten Iterationsschritt vorab berechnen
  i=0
  sinus.set_A(A)
  sinus.set_omega(omega)
  sin_vec[i] = sinus.calc(t_vec[i])

  # Erste Plot-Ausgabe erzeugen
  sin_plot.init_plot(t_vec[:i], sin_vec[:i])
  # Jetzt den Plot mit kurzer Pause ausgeben ohne anzuhalten ...
  plt.pause(0.1)

  # ... und jetzt die restlichen Stützpunkte iterativ berechnen und nach
  # jedem 10. Schritt den Plot aktualisieren
  for i in range(1,n_gesamt):
    t_vec[i] += t_vec[i-1]+dt
    # omega durch"wobbeln"
    omega += domega
    sinus.set_omega(omega)
    # A durch"wobbeln"
    A += dA
    sinus.set_A(A)
    sin_vec[i] = sinus.calc(t_vec[i])

    if i % 10 == 0:
      sin_plot.update_plot(t_vec[:i], sin_vec[:i])
      plt.pause(0.1)
#---------------------------------------------