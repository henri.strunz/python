import numpy as np
import matplotlib.pyplot as plt

def formen_berechnen(x):
    print("Wir berechnen zu allerest alle Sinusse, nachedem wir 2pi aufgeteilt haben \n")
    # ---- Berechnung --------
    # x als np-array mit 100 Werten im Intervall 0..2 PI anlegen ...
    sin_x1 = np.sin(x)
    sin_x2 = np.sin(3*x)
    sin_x3 = (np.sin(x))**2
    return sin_x1, sin_x2, sin_x3

    
    
def plotten(x, sin_x1, sin_x2, sin_x3):
    (fig, ax) = plt.subplots()
    print("Jetzt plotten wir alle Sinusse und stellen sie dar\n")
    # dann plotten
    ax.plot(x, sin_x1, label='sin(x)', color = 'r')
    ax.plot(x, sin_x2, label='sin(3x)', color = 'blue', linestyle = '-')
    ax.plot(x, sin_x3, label='sin(x)^2', color = 'black', marker='+', linestyle = '')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.grid()
    ax.legend()
    # und anzeigen
    plt.show()
# ... und den Sinus für alle Werte von x berechnen
    
x = np.linspace(0, 2*np.pi, 100)
sinx1, sinx2, sinx3 = formen_berechnen(x)
plotten(x,sinx1,sinx2,sinx3)
#x, sin_x1, sin_x2, sin_x3 = formen_berechnen()
# ---- Anzeige --------
# figure anlegen
#(fig, ax) = plt.subplots()

# dann plotten
#ax.plot(x, sin_x1)
#ax.plot(x, sin_x2)
#ax.plot(x, sin_x3)
# und anzeigen
#plt.show()