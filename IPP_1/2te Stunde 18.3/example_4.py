import matplotlib.pyplot as plt

import math

def plot_fct_values(x_list, y_list):
  (fig, ax) = plt.subplots()
  ax.set_xlabel('x')
  ax.set_ylabel('y')
  ax.grid()
  ax.set_title('Unser erster Plot mit Python')
  ax.plot(x_list, y_list, color='r', linestyle=':')
  plt.show()

def read_fct_values(datei_name):
  fd = open(datei_name,'r')

# Jetzt lesen wir alle Zeilen der Datei auf einmal ein. Die Funktion
# 'readlines()' liefert uns eine Liste zurück, in der jeder Eintrag einer
# Zeile der Datei entspricht.
  Lines = fd.readlines()

# wir erzeugen 2 leere Liste für x und y ...
  x_list = []
  y_list = []
# ... und arbeiten nun die Einträge der String-Liste
# nacheinander ab
  for Line in Lines:
  # 'split()' spaltet eine String in einzelne Wörter auf, als Trennzeichen
  # werden alle sog. "Whitespace-Character" angenommen (' ' und ',' und '.' etc.)
    line_elements = Line.split()
  # jetzt wandeln wir die Teilstrings wieder in numerische Werte für x und y
    x_list.append(float(line_elements[0]))
    y_list.append(float(line_elements[1]))
  
# und geben das Ganze zur Kontrolle auf der Konsole aus
  for i in range(0, len(x_list)):
    print(x_list[i], y_list[i])


  return x_list, y_list

datei_name = input("Gib mir einen Datein namen: ")
"""
datei_name = input("Gib mir einen Datein namen: ")
anzahl_s = int(input("Wie viele Stützstellen: "))
list_x2 = []
list_sin = []
list_x = range(0,anzahl_s)
fd = open(datei_name, 'w')
for i in list_x:
  list_x2.append(i*2*math.pi*(1/anzahl_s))
  list_sin.append(math.sin(list_x2[i]))
  fd.write(str(list_x2[i]) +" "+ str(list_sin[i]) + "\n")

#fd.write(str(anzahl_s) + ' Datensätze in der Datei ' + datei_name +' gespeichert ')
fd.close
"""

# Datei öffnen


x, y = read_fct_values(datei_name)
print(x, y)
plot_fct_values(x, y)