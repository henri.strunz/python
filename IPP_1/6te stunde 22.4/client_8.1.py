"""
Einfacher UDP-Client, der eine Nachricht sendet, auf eine Antwort sendet und sich dann beendet.
"""

# Für die Sockets gibt es natürlich ein eigenes Modul
import socket
import numpy as np
import matplotlib.pyplot as plt

class Sinus():
  def __init__(self, A=1.0, omega=1.0):
    # Amplitude
    self.A = A
    # Kreisfrequenz
    self.omega = omega

  # "Setter"-Funktion für die Amplitude
  def set_A(self, A):
    self.A = A

  # "Setter"-Funktion für die Kreisfrequenz
  def set_omega(self, omega):
    self.omega = omega

  # Berechnung des Sinus
  def calc(self,t):
    return self.A * np.sin(self.omega*t)



if __name__ == '__main__':
        
    
    # Wir nutzen das Loopback-Device zur rechnerinternen Kommunikation ...
    localIP     = "127.0.0.1"
    # ... und einen nicht belegten Port
    localPort   = 1234
    # IP und Port müssen wir als Tupel zusammenfassen
    serverAddressPort   = ("127.0.0.1", 1234)
    # Das ist die maximale Paketlänge für den Empfang ...
    bufferSize  = 1024
    # Jetzt legen wir einen Socket für die UDP an ...
    UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    
    sinus_dtype = np.dtype([('id',np.int32),
                         ('x', np.float32),
                         ('y', np.float32)])


    
    n_gesamt = 1000
      # Darstellungsintervall
    t_gesamt = 2.0*np.pi
    # Inkrement zwischen 2 Stützpunkten
    dt = t_gesamt / n_gesamt
    # Vektor für Zeitwerte
    t_vec = np.zeros(n_gesamt)
    # Vektor für Ergebniswerte
    sin_vec = np.zeros_like(t_vec)
    # Sinus-Objekt anlegen ...
    sinus = Sinus()
    # Wir ergänzen ein paar Parameter für die Sinus-Funktion
    omega = 0.0
    domega = 0.025
    A = 1.0
    dA = -0.001
    # ... und den ersten Iterationsschritt vorab berechnen
    i=0

    sinus.set_A(A)
    sinus.set_omega(omega)
    sin_vec[i] = sinus.calc(t_vec[i])   
    sinus_1 = np.zeros(1, dtype = sinus_dtype)

    sinus_1['id'] = 1
    sinus_1['x'] = 0
    sinus_1['y'] = sinus.calc(t_vec[i])
    bytesToSend = sinus_1.tostring()
    UDPClientSocket.sendto(bytesToSend, serverAddressPort)

    for i in range(1,n_gesamt):
        sinus_1['id']= 2
        sinus_1['x'] += dt
        t_vec[i] += t_vec[i-1]+dt
        # omega durch"wobbeln"
        omega += domega
        sinus.set_omega(omega)
        # A durch"wobbeln"
        A += dA
        sinus.set_A(A)
        sinus_1['y']= sinus.calc(sinus_1['x'])
        bytesToSend = sinus_1.tostring()
        UDPClientSocket.sendto(bytesToSend, serverAddressPort)
        print(sinus_1)
    



    # ... und dann auf die Antwort warten
    # Die Funktion recvfrom() kehrt nur zurück, wenn eine Nachricht empfangen wurde
    # oder ein Fehler aufgetreten ist ...