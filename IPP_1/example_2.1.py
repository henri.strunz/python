# Aufgabe 2.2

def fak(n):
  # Startwert der Berechnung
  # Wir berechnen mindestens immer 0!
  i_n = 0   # i_n = Iterator i von 0 bis n
  # Hiermit merken wir uns jeweils den zuletzt berechneten
  # Fakultätswert
  n_fak = 1 # 0! = 1

  # wir überprüfen, ob wir n noch nicht erreicht haben ...
  while  i_n < n:
    # ... und solange gehen wir eine "Zeile weiter" => n! = (n-1)! n
    i_n = i_n + 1
    n_fak = n_fak * i_n

  return n_fak
# Kleinen Text ausgeben, damit der Nutzer weiß, was hier passiert
print('Berechnung von sin(x) mittels endlicher Reihe')
pi = float(3.1415936)
# Wir lesen x von der Konsole ein, dabei können wir 'input()' einen
# String übergeben, mit dem wir dem Nutzer sagen, was wir von ihm wollen
x = float(input('Geben Sie x in Grad ein! '))
x *= pi/180
n= 0
sin_x= 0
# und dann die sin-Näherung mit den ersten 3 Reihengliedern berechnen
while n<10:
    sin_x += ((-1)**n)*(x**(2*n+1))/fak(2*n+1)
    n += 1

# augeben sollten wir das Ergebnis natürlich auch noch
print('sin(',x,') = ', sin_x)