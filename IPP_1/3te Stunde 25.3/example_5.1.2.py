import numpy as np
import matplotlib.pyplot as plt

def formen_berechnen(x):
    print("Wir berechnen zu allerest alle Sinusse, nachedem wir 2pi aufgeteilt haben \n")
    # ---- Berechnung --------
    # x als np-array mit 100 Werten im Intervall 0..2 PI anlegen ...
    e = np.exp(-0.5*x)
    sin_x1 = np.sin(x)
    sin_x2 = np.sin(3*x)
    sin_x3 = (np.sin(x))**2
    x4 = np.sin(10*x)*e
    x5 = e
    x6 = -e
    return sin_x1, sin_x2, sin_x3, x4, x5, x6

    
    
def plotten(x, sin_x1, sin_x2, sin_x3, x4, x5, x6):
    print("Jetzt plotten wir alle Sinusse und stellen sie dar\n")
    # dann plotten
    fig, axs = plt.subplots(2, 2, figsize=(10, 8))  # 3 Zeilen, 1 Spalte und Größe 8,6 außerdem ein Array
    # dann plotten
    axs[0][0].plot(x, sin_x1, label='sin(x)', color='r')       # so die einzelnen Plots ansprechen
    axs[0][0].set_title('sin(x)')
    axs[1][0].plot(x, sin_x2, label='sin(3x)', color='b', linestyle='-')
    axs[1][0].set_title('sin(3x)')
    axs[0][1].plot(x, sin_x3, label='sin(x)^2', color='black', marker='+', linestyle='')
    axs[0][1].set_title('sin(x)^2')
    axs[1][1].plot(x, x4, label = 'sin(10x)*e^-0.5x', color = 'blue')
    axs[1][1].plot(x, x5, label = 'e^-0.5x', color = 'blue', linestyle = '-')
    axs[1][1].plot(x, x6, label = '-e^-0.5x', color = 'blue', linestyle = '-')
    axs[1][1].set_title('Exponentialfunktionen')
    for row in axs:
        for ax in row:
            ax.set_xlabel('x')          #für jedes Plot die Achsen beschriften und formatieren und Legende machen
            ax.set_ylabel('y')
            ax.grid()
            ax.legend()
    plt.tight_layout()  # Platzierung der Subplots optimieren
    # und anzeigen
    plt.show()
    # und anzeigen
# ... und den Sinus für alle Werte von x berechnen
    
x = np.linspace(0, 2*np.pi, 100)
sinx1, sinx2, sinx3, x4, x5, x6 = formen_berechnen(x)
plotten(x,sinx1,sinx2,sinx3, x4, x5, x6)