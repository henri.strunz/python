model = {
    "Zeitschritt" : 0.05,
    "Verstärkungsfaktor" : 1,
    "Zeitkonstante" : 2.5
}
plot = {
    "width" : 8,
    "height" : 10,
    "linestyle.soll" : 'dotted',
    "linestyle.s.ist" : 'dashdot',
    "marker.s.ist" : '',
    "colour.vs" : 'blue',
    "colour.vi" : 'red',
    "colour.a" : 'green',
    "colour.s" : 'black'
}
data = {
    "dataname" : 'Soll.Ist'
}