import example_6_1_dict as ex6
import numpy as np
import matplotlib.pyplot as plt

class MotionPlot():
    def __init__(self):
       self.fig, self.axs = plt.subplots(3, 1, figsize=(ex6.plot['width'], ex6.plot['height']))

    def init_plot(self, tk_list, vk_s, vk_i, ak_i, sk_i):
        self.vk_s, = self.axs[0].plot(tk_list, vk_s, label='vi(t)', color= ex6.plot['colour.vs'], linestyle = ex6.plot['linestyle.soll']) # so die einzelnen Plots ansprechen
        self.axs[0].set_title('v soll')
        self.axs[0].set_xlim(-2,+65)
        self.axs[0].set_ylim(-2,+35)
        self.vk_i, =self.axs[0].plot(tk_list, vk_i, label='vs(t)', color=ex6.plot['colour.vi'])       
        self.axs[0].set_title('v ist')
        self.sk_s, =self.axs[1].plot(tk_list, sk_i, label='s(t)', color=ex6.plot['colour.s'], linestyle = ex6.plot['linestyle.s.ist'], marker = ex6.plot['marker.s.ist'])
        self.axs[1].set_title('s ist')
        self.axs[1].set_xlim(-2,+65)
        self.axs[1].set_ylim(-2,+900)
        self.ak_s, =self.axs[2].plot(tk_list, ak_i, label='a(t)', color=ex6.plot['colour.a'])
        self.axs[2].set_title('a ist')
        self.axs[2].set_xlim(-2,+65)
        self.axs[2].set_ylim(-30,+30)
        for row in self.axs:
            row.set_xlabel('x')          #für jedes Plot die Achsen beschriften und formatieren und Legende machen
            row.set_ylabel('y')
            row.grid()
            row.legend()
        plt.tight_layout()  # Platzierung der Subplots optimieren
        # und anzeigen
        plt.pause(0.1)

    def update_plot(self, t_vec, v_soll, v_ist, a_ist, s_ist):
        self.vk_s.set_data(np.array(t_vec), np.array(v_soll))
        self.vk_i.set_data(np.array(t_vec), np.array(v_ist))
        self.sk_s.set_data(np.array(t_vec), np.array(s_ist))
        self.ak_s.set_data(np.array(t_vec), np.array(a_ist))

# Klasse zur Repräsentation der Objektbewegung
# (Berechnung von v_ist, a_ist, s_ist)
class MotionObject():
    def __init__(self, s_i, v_i, a_i):
        self.vs = 0
        self.s_ist = s_i
        self.v_ist = v_i
        self.a_ist = a_i
        self.v_d = 0
  
    def set_v_soll(self, v_soll):
        self.vs = v_soll

    def move(self, dt):
        self.s_ist=(self.s_ist+dt*self.v_ist)  #neue Strecke vor neuer geschwindikeit --> sonst rechnen wir mit vk+1
        self.v_d = self.v_ist
        self.v_ist=(self.v_ist+(ex6.model["Verstärkungsfaktor"]*self.vs-self.v_ist)*(dt/ex6.model["Zeitkonstante"]))
        self.a_ist=((self.v_ist - self.v_d)/dt)
        return self.s_ist, self.v_ist, self.a_ist
    

def datei_fuellen(datei_name):
    fd = open(datei_name, 'w')
    data = np.array([[0, 12.5], [15.5, 0], [28.5, 31], [43, 22.5], [52, 0], [60, 0]])  
    np.savetxt(datei_name, data, delimiter=',', fmt='%f')       
    fd.close()

def datei_lesen(datei_name):
    fd = open(datei_name, 'r')
    data = np.loadtxt(datei_name, delimiter= ',')
    t_list = data[:, 0]
    vsoll_list = data[:, 1]
    fd.close()
    return t_list, vsoll_list

if __name__ == '__main__':
    # Datei erstellen
    datei_fuellen(ex6.data["dataname"])

    # Datei auslesen
    t_list, vsoll_list = datei_lesen(ex6.data["dataname"])
    tk_list= np.arange(0, t_list[-1]+ex6.model["Zeitschritt"], ex6.model["Zeitschritt"])
    si = np.zeros(len(tk_list))
    vi = np.zeros(len(tk_list))    
    ai= np.zeros(len(tk_list))            #arrays brauchen geliche länge wie bei erstellung der Plots
    vsk = np.zeros(len(tk_list))
    # Initialisierung der Objektbewegung
    counter = 0
    # Initialisierung der Plotklasse
    plot = MotionPlot()
    plot.init_plot(tk_list[:counter], vsk[:counter], vi[:counter], ai[:counter], si[:counter])
    # Schleife über die Zeitschritte
    
    
    for t in range(0, len(t_list)-1, 1): 
        current_tk_list = tk_list[:counter+1]
        for tk in np.arange(t_list[t]+ex6.model["Zeitschritt"], t_list[t+1]+ex6.model["Zeitschritt"], ex6.model["Zeitschritt"]):
            counter += 1
            obj = MotionObject(si[counter-1], vi[counter-1], ai[counter-1])
            obj.set_v_soll(vsoll_list[t])
            s,v,a = obj.move(ex6.model["Zeitschritt"])
            si[counter]= s
            vi[counter]= v
            ai[counter]= a
            vsk[counter] = vsoll_list[t]
            if counter%100 == 0:
                plot.update_plot(tk_list[:counter], vsk[:counter], vi[:counter], ai[:counter], si[:counter])
                plt.pause(0.1)
    plt.show()