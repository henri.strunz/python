# abschlussaufgabe
import numpy as np
import matplotlib.pyplot as plt
def datei_fuellen(datei_name):
    fd = open(datei_name, 'w')
    data = np.array([[10, 2, 2], [20, -1, -2], [30, -3, -1], [40, 4, 1], [50, 2, 0]])  
    np.savetxt(datei_name, data, delimiter=',', fmt='%d')       
    fd.close()

def datei_lesen(datei_name):
    fd = open(datei_name, 'r')
    data = np.loadtxt(datei_name, delimiter= ',')
    t_list = data[:, 0]
    a_list = data[:, 1]
    ad_list = data[:, 2]
    print(t_list, a_list, ad_list)
    print(data)
    fd.close()
    return t_list, a_list, ad_list

def ausrechnen(t_list, a_list, tk_list):
    sk_list = [0]
    vk_list = [0]
    ak_list = [a_list[0]]
    u_1 = [a_list[0]]   
    


    for tk in tk_list:
        m1 = np.array([[1, 0.1 , ((0.1)**2)/2],
                       [0, 1, 0.1],
                       [0, 0, 1]], dtype=np.float32)
        if tk == t_list[0]:
            ak_list[-1] += a_list[1]
            u_1.append(a_list[1])
        elif tk == t_list[1]:
            ak_list[-1] += a_list[2]
            u_1.append(a_list[2])
        elif tk == t_list[2]:
            ak_list[-1] += a_list[3] 
            u_1.append(a_list[3])
        elif tk == t_list[3]:
            ak_list[-1] += a_list[4] 
            u_1.append(a_list[4])   
        else: 
            u_1.append(0)
        result = np.matmul(m1, [sk_list[-1], vk_list[-1], ak_list[-1]])
        (sk, vk, ak) = result
        sk_list.append(sk)
        vk_list.append(vk)
        ak_list.append(ak)
              
            
        
    return sk_list, vk_list, ak_list, u_1

def plotten(tk_list, sk_list, vk_list, ak_list, u_list):
    fig, axs = plt.subplots(4, 1, figsize=(8, 6))  # 3 Zeilen, 1 Spalte und Größe 8,6 außerdem ein Array
    # dann plotten
    axs[0].plot(tk_list, sk_list, label='s(t)', color='red')       # so die einzelnen Plots ansprechen
    axs[0].set_title('s(t)')
    axs[1].plot(tk_list, vk_list, label='v(t)', color='blue')
    axs[1].set_title('v(t)')
    axs[2].plot(tk_list, ak_list, label='a(t)', color='black')
    axs[2].set_title('a(t)')
    axs[3].plot(tk_list, u_list, label = 'u(t)', color = 'green')
    axs[3].set_title('u(t)')
    for row in axs:
        row.set_xlabel('x')          #für jedes Plot die Achsen beschriften und formatieren und Legende machen
        row.set_ylabel('y')
        row.grid()
        row.legend()
    plt.tight_layout()  # Platzierung der Subplots optimieren
    # und anzeigen
    plt.show()
    
    
    return


datei_n = input("Gib mir bitte den Namen der Datei! ")
datei_fuellen(datei_n)
t_list, a_list, ad_list = datei_lesen(datei_n)

tk_list= np.arange(0, t_list[t_list.size-1], 0.1)
tk2_list = np.arange(0, t_list[t_list.size-1]+0.1, 0.1)
u_1 = np.arange(0, t_list[t_list.size-1], 0.1)

skl, vkl, akl, u1 = ausrechnen(t_list, a_list, tk_list)
plotten(tk2_list, skl, vkl, akl, u1)